##
## EPITECH PROJECT, 2023
## corewar
## File description:
## Definition of the fantasy console project recipes
##

# Project definitions

CO_FAN_SOURCE_DIR	:= source
CO_FAN_INCLUDE_DIR	:= .
CO_FAN_DOC_DIR 		:= doc
CO_FAN_TEST_DIR     := tests

CO_FAN_SOURCE		:= $(shell find $(CO_FAN_SOURCE_DIR) -name "*.cpp")
CO_FAN_OBJECT		:= $(CO_FAN_SOURCE:.cpp=.o)
CO_FAN_TARGET		:= corebleeps

# Project libraries

SDL2_CFLAGS			:= $(shell sdl2-config --cflags)
SDL2_LDFLAGS 		:= $(shell sdl2-config --libs)

ifeq ($(SDL2_CFLAGS)$(SDL2_LDFLAGS),)
$(error SDL2 seems to be missing. Install it first before compiling.)
endif

CXXFLAGS 			+= $(SDL2_CFLAGS)
LDFLAGS 			+= $(SDL2_LDFLAGS)

# Named targets

all: $(CO_FAN_TARGET)

clean:
	$(RM) $(CO_FAN_OBJECT)

fclean: clean
	$(RM) $(CO_FAN_TARGET)
	$(RM) -r $(CO_FAN_DOC_DIR)

re: fclean all

test:
	echo dummy

# doc:
# 	doxygen

# Compiler options

CXXFLAGS			+= -std=c++23 -W -Wall -Wextra
CPPFLAGS			+= -I$(CO_FAN_INCLUDE_DIR)

# Generic targets

$(CO_FAN_TARGET): $(CO_FAN_OBJECT)
	$(CXX) $(CPPFLAGS) -o $@ $^ $(LDFLAGS)
