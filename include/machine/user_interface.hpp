#ifndef CO_FAN_MACHINE_USERINTERFACE_HPP
#define CO_FAN_MACHINE_USERINTERFACE_HPP

#include "include/machine/memory.hpp"

#include <SDL2/SDL.h>

#include <chrono>

namespace CoreBleeps {
    class UserInterface {
    public:
        static constexpr double REFRESH_RATE = 60.0;
        static constexpr std::chrono::nanoseconds REFRESH_TIME =
            std::chrono::nanoseconds(static_cast<int64_t>(1000000000.0 / UserInterface::REFRESH_RATE));

    protected:
        DisplayMemoryArea  *display_area;
        RegisterMemoryArea *input_area;

        SDL_Window   *window;
        SDL_Renderer *renderer;

        SDL_Texture *area_texture;
    
    public:
        UserInterface(DisplayMemoryArea *display_area, RegisterMemoryArea *input_area, int scale);
        ~UserInterface();

    private:
        void update_area_texture();

    public:
        void run();
    };
};

#endif
