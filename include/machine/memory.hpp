#ifndef CO_FAN_MACHINE_MEMORY_HPP
#define CO_FAN_MACHINE_MEMORY_HPP

#include <list>
#include <tuple>
#include <string>
#include <bit>

#include <cstdint>

namespace CoreBleeps {
    class MemoryArea {
    public:
        virtual std::string & name() = 0;
        virtual uint32_t      size() = 0;

        virtual uint8_t get(uint32_t address) = 0;
        virtual void set(uint32_t address, uint8_t value) = 0;

        virtual ~MemoryArea() = default;
    };

    class UserMemoryArea : public MemoryArea {
    protected:
        std::string my_name;
        uint32_t    my_mem_size;

        uint8_t     *my_mem;

    public:
        std::string & name() override;
        uint32_t      size() override;

        UserMemoryArea(std::string name, uint32_t size);
        ~UserMemoryArea() override;
        
        uint8_t get(uint32_t address) override;
        void set(uint32_t address, uint8_t value) override;

        void copy_from(const uint8_t *buffer, uint32_t size, uint32_t target_address);
        void copy_to(uint32_t source_address, uint32_t size, uint8_t *buffer);
    };

    class RegisterMemoryArea : public UserMemoryArea {
    protected:
        std::string my_name;

    public:
        RegisterMemoryArea(std::string name, int count);

        uint32_t get_register(int id);
        void set_register(int id, uint32_t value);
    };

    class DisplayMemoryArea : public UserMemoryArea {
    protected:
        int my_width;
        int my_height;
    
    public:
        DisplayMemoryArea(std::string name, int width, int height);
        
        int width();
        int height();
    };

    class MemoryMap {
    public:
        static const uint8_t DEFAULT_RAM_READVALUE = ~0;

    protected:
        using space_entry = std::tuple<uint32_t, MemoryArea *>;
        
        std::list<space_entry> *space;

    public:
        MemoryMap();
        ~MemoryMap();

        void map(uint32_t starting_address, MemoryArea *area);
        void unmap(uint32_t starting_address);

    protected:
        const space_entry * get_area(uint32_t address) const;

    public:
        template <typename T, std::enable_if_t<std::is_integral_v<T>>...>
        T get(uint32_t address)
        {
            static_assert(1 <= sizeof(T) && sizeof(T) <= 4, "only expecting 8-bit to 32-bit integrals");

            uint8_t bytes[sizeof(T)];            
            for (size_t i = 0; i < sizeof(T); i++) {
                const space_entry *entry = this->get_area(address + i);

                if (!entry)
                    bytes[i] = MemoryMap::DEFAULT_RAM_READVALUE;
                else {
                    uint32_t rel_address = address + i - std::get<0>(*entry);
                    MemoryArea *area     = std::get<1>(*entry);

                    bytes[i] = area->get(rel_address);
                }
            }

            T final_value = *((T *) bytes);
            return std::endian::native == std::endian::little ? std::byteswap(final_value) : final_value;
        }

        template <typename T, std::enable_if_t<std::is_integral_v<T>>...>
        void set(uint32_t address, T value)
        {
            static_assert(1 <= sizeof(T) && sizeof(T) <= 4, "only expecting 8-bit to 32-bit integrals");

            for (size_t i = 0; i < sizeof(T); i++) {
                const space_entry *entry = this->get_area(address);

                if (!entry)
                    continue;
                
                uint8_t byte         = (value >> ((sizeof(T) - i - 1) * 8)) & 0xff;
                uint32_t rel_address = address - std::get<0>(*entry);
                MemoryArea *area     = std::get<1>(*entry);

                area->set(rel_address + i, byte);
            }
        }
    };
}

#endif
