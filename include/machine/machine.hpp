#ifndef CO_FAN_MACHINE_HPP
#define CO_FAN_MACHINE_HPP

#include "include/program.hpp"

#include "include/machine/processor.hpp"
#include "include/machine/user_interface.hpp"

namespace CoreBleeps {
    class Machine {
    public:
        static constexpr double TICK_FREQUENCY = 2000000000.0;
        static constexpr std::chrono::nanoseconds TICK_TIME =
            std::chrono::nanoseconds(static_cast<int64_t>(1000000000.0 / Machine::TICK_FREQUENCY));

    protected:
        Processor *processor;
        
        UserMemoryArea     *program_area;
        DisplayMemoryArea  *display_area;
        RegisterMemoryArea *input_area;

    public:
        Machine(Program *program);
        ~Machine();

        void run();

    private:
        static void run_display(Machine *context);
        static void run_cpu(Machine *context);
    };
};

#endif
