#ifndef CO_FAN_MACHINE_INSTRUCTION_HPP
#define CO_FAN_MACHINE_INSTRUCTION_HPP

#include <list>
#include <memory>

#include <cstddef>
#include <cstdint>

namespace CoreBleeps {
    class Processor;

    enum ProcessorOpArgType {
        ARG_REGISTER          = 1 << 0, // 0b01,
        ARG_REGISTER_RELATIVE = 1 << 1,
        ARG_DIRECT            = 1 << 2, // 0b10,
        ARG_DIRECT_RELATIVE   = 1 << 3,
        ARG_INDIRECT          = 1 << 4, // 0b11,
        ARG_INDIRECT_VALUE    = 1 << 5,
        ARG_WIDE              = 1 << 6,
        ARG_UNDEFINED         = 0       // 0b00
    };

    inline ProcessorOpArgType operator |(ProcessorOpArgType a, ProcessorOpArgType b) {
        return static_cast<ProcessorOpArgType>(static_cast<int>(a) | static_cast<int>(b));
    }

    inline ProcessorOpArgType operator &(ProcessorOpArgType a, ProcessorOpArgType b) {
        return static_cast<ProcessorOpArgType>(static_cast<int>(a) & static_cast<int>(b));
    }

    class ProcessorOpArg {
    public:
        virtual uint32_t get() = 0;
        virtual void     set(uint32_t value) = 0;
    };

    class ProcessorOpRegisterArg : public ProcessorOpArg {
    protected:
        Processor *processor;
        uint8_t    reg_id;
    
    public:
        ProcessorOpRegisterArg(Processor *processor, uint8_t reg_id);
    
        uint32_t get() override;
        void     set(uint32_t value) override;
    };

    class ProcessorOpLiteralArg : public ProcessorOpArg {
    protected:
        uint32_t value;

    public:
        ProcessorOpLiteralArg(uint32_t literal_value);
        ProcessorOpLiteralArg(int32_t relative_value);
        ProcessorOpLiteralArg(int16_t relative_value);

        uint32_t get() override;
        void     set(uint32_t value) override;
    };

    class ProcessorOpPointerArg : public ProcessorOpArg {
    protected:
        Processor *processor;
        uint32_t  address;
    
    public:
        ProcessorOpPointerArg(Processor *processor, uint32_t linear_address);
        ProcessorOpPointerArg(Processor *processor, int32_t relative_address);
        ProcessorOpPointerArg(Processor *processor, int16_t relative_address);

        uint32_t get() override;
        void     set(uint32_t value) override;
    };

    using ProcessorOpArgs = std::list<std::shared_ptr<ProcessorOpArg>>;

    struct ProcessorOpDef {
        const char *name;
        size_t     cycle_count;

        std::list<ProcessorOpArgType> arg_types;

        bool (*impl)(Processor *processor, ProcessorOpArgs args);
    };
};

#endif
