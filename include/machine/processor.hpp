#ifndef CO_FAN_MACHINE_PROCESSOR_HPP
#define CO_FAN_MACHINE_PROCESSOR_HPP

#include "include/machine/memory.hpp"
#include "include/machine/instruction.hpp"

#include <list>
#include <map>
#include <chrono>

#include <cstddef>
#include <cstdint>

namespace CoreBleeps {
    class Processor {
    public:
        static const std::map<uint8_t, ProcessorOpDef> INSTRUCTION_SET;

    private:
        static bool instruction_ld(Processor *processor, ProcessorOpArgs args);
        static bool instruction_st(Processor *processor, ProcessorOpArgs args);
        static bool instruction_add(Processor *processor, ProcessorOpArgs args);
        static bool instruction_sub(Processor *processor, ProcessorOpArgs args);
        static bool instruction_and(Processor *processor, ProcessorOpArgs args);
        static bool instruction_or(Processor *processor, ProcessorOpArgs args);
        static bool instruction_xor(Processor *processor, ProcessorOpArgs args);
        static bool instruction_zjmp(Processor *processor, ProcessorOpArgs args);
        static bool instruction_ldi(Processor *processor, ProcessorOpArgs args);
        static bool instruction_sti(Processor *processor, ProcessorOpArgs args);

        static bool instruction_nop(Processor *processor, ProcessorOpArgs args);

        static bool instruction_push(Processor *processor, ProcessorOpArgs args);
        static bool instruction_pop(Processor *processor, ProcessorOpArgs args);
        static bool instruction_call(Processor *processor, ProcessorOpArgs args);
        static bool instruction_ret(Processor *processor, ProcessorOpArgs args);
        static bool instruction_lsp(Processor *processor, ProcessorOpArgs args);
        static bool instruction_ssp(Processor *processor, ProcessorOpArgs args);
        static bool instruction_jmp(Processor *processor, ProcessorOpArgs args);
        static bool instruction_nzjmp(Processor *processor, ProcessorOpArgs args);
        static bool instruction_cjmp(Processor *processor, ProcessorOpArgs args);
        static bool instruction_shl(Processor *processor, ProcessorOpArgs args);
        static bool instruction_shr(Processor *processor, ProcessorOpArgs args);
        static bool instruction_not(Processor *processor, ProcessorOpArgs args);
        static bool instruction_cmp(Processor *processor, ProcessorOpArgs args);
        static bool instruction_ldf(Processor *processor, ProcessorOpArgs args);
        static bool instruction_stf(Processor *processor, ProcessorOpArgs args);
        static bool instruction_clc(Processor *processor, ProcessorOpArgs args);
        static bool instruction_stc(Processor *processor, ProcessorOpArgs args);
        static bool instruction_gjmp(Processor *processor, ProcessorOpArgs args);
        static bool instruction_ljmp(Processor *processor, ProcessorOpArgs args);
        static bool instruction_ejmp(Processor *processor, ProcessorOpArgs args);
        static bool instruction_inc(Processor *processor, ProcessorOpArgs args);
        static bool instruction_dec(Processor *processor, ProcessorOpArgs args);

    public:
        static const uint32_t FLAG_CARRY   = 1 << 0;
        static const uint32_t FLAG_ZERO    = 1 << 1;
        static const uint32_t FLAG_EQUAL   = 1 << 2;
        static const uint32_t FLAG_GREATER = 1 << 3;
        static const uint32_t FLAG_RUNNING = 1 << 31;

        uint32_t pc;
        uint32_t sp;
        uint32_t flags;
        uint32_t gp[16];

        bool                  opcode_invalid;
        const ProcessorOpDef *opcode;
        size_t                opcode_length;
        size_t                cycles_left;

        MemoryMap      *memory;

        Processor();
        ~Processor();

    protected:
        void fetch();

    private:
        ProcessorOpArgs decode();

    public:
        void tick();
        void step();
        void run(std::chrono::nanoseconds tick_delay);
        void halt();

        bool get_flag(uint32_t flag)
        {
            return this->flags & flag;
        }

        void set_flag(uint32_t flag, bool value)
        {
            if (value)
                this->flags |= flag;
            else
                this->flags &= ~flag;
        }
    };
};

#endif
