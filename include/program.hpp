#ifndef CO_FAN_PROGRAM_HPP
#define CO_FAN_PROGRAM_HPP

#include <string>
#include <fstream>

#include <cstdint>

namespace CoreBleeps {
    const int HEADER_MAGIC        = 0xea83f3;
    const int HEADER_NAME_SIZE    = 128;
    const int HEADER_COMMENT_SIZE = 2048; 

    struct ProgramHeader {
        uint32_t magic;
        char     program_name[HEADER_NAME_SIZE + 1];
        uint32_t program_size;
        char     program_comment[HEADER_COMMENT_SIZE + 1];
    };

    class Program {
    protected:
        std::string program_name;
        uint32_t    program_size;
        std::string program_comment;
        uint8_t    *program_code;

    public:
        const std::string & name();
        uint32_t            size();
        const std::string & comment();
        const uint8_t *     code();

        Program(std::ifstream &stream);
        ~Program();
    };
};

#endif
