#!/usr/bin/env python3

import sys
import struct

from PIL import Image

if len(sys.argv) != 3:
    print(f"usage: {sys.argv[0]} image output_source", file=sys.stderr)

input_image = Image.open(sys.argv[1])
output_file = open(sys.argv[2], "wb")

output_file.write(b"\x00\xea\x83\xf3")
output_file.write(struct.pack(">132s1L2052s",
    bytes(sys.argv[1], encoding='utf-8'),
    320 * 200 * 9 + 3,
    b"Converted with the bonus program in the bonus project"
))

input_image = input_image.resize([320, 200])

for i in range(320 * 200):
    vid_address = 0xfffc0000 + i * 4

    pixel     = input_image.getpixel((i % 320, i // 320))
    raw_pixel = struct.pack(">BBBBBBL", 0x20, 0b10100000, 0, pixel[2], pixel[1], pixel[0], vid_address)
    output_file.write(raw_pixel)

struct.pack(">BH", 0x09, 0)

output_file.close()
