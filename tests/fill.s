.name    "Flood fill the screen!"
.comment "The very first program of this fantasy console..."

# Screen @ 0xfffc0000 - layout is XXBBGGRR

# Setup code
lsp     %0x6000
ld      %0xfffc0000, r1     # Base
ld      %0, r2              # Offset
ld      %0x00000000, r3     # Colour
ld      %0,          r4     # Dot counter

ld      %4, r14             # Offset increment
ld      %0x00010101, r15    # Colour increment

# Main loop
fill_loop:
add     r1, r2, r16         # Calculate the linear address from base + offset
stf     r3, r16             # Set the actual pixel color

cmp     r2, %0x3ffff        # Check if we have reached the end of the screen
ejmp    %:end
gjmp    %:end               # Yes, end the program

inc     r4                  # Increment the dot count
cmp     r4, %320            # Check if we have reached the end of a line
ejmp    %:update_color

fill_loop_continue:
add     r2, r14, r2         # Increment the offset
jmp     %:fill_loop         # Do it all again!

end:
# End of the program
jmp     %:end

update_color:
add     r3, r15, r3         # Increment the colour components
ld      %0, r4
jmp     %:fill_loop_continue
