.name    "Input from the keyboard"
.comment "Keycode demo w/ the screen"

# Input @ 0xfffb0000
# Screen @ 0xfffc0000

loop:
ldf     %0xfffb0000, r1
shl     r1, %4, r1
stf     r1, %0xfffc0000
stf     r1, %0xfffc0004
stf     r1, %0xfffc0008
stf     r1, %0xfffc0500
stf     r1, %0xfffc0504
stf     r1, %0xfffc0508

jmp     %:loop
