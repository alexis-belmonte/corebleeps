#include "include/machine/processor.hpp"
#include "include/machine/machine.hpp"

#include <iostream>
#include <fstream>
#include <format>

#include <chrono>
#include <thread>

int main(int argc, char *argv[])
{
    if (argc != 2) {
        std::cerr << std::format("{}: expected a program file", argv[0]) << std::endl;
        return 1;
    }

    std::ifstream stream(argv[1], std::ios::binary | std::ios::ate);
    if (stream.fail()) {
        std::cerr << std::format("{}: cannot open program file", argv[0]) << std::endl;
        return 1;
    }

    CoreBleeps::Program  *program   = new CoreBleeps::Program(stream);
    CoreBleeps::Machine  *machine = new CoreBleeps::Machine(program);

    machine->run();

    delete machine;
    delete program;
    return 0;
}
