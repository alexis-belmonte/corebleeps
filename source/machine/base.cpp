#include "include/machine/machine.hpp"

#include "include/machine/processor.hpp"

#include "include/program.hpp"

#include <stdexcept>

#include <cstddef>

using namespace CoreBleeps;

constexpr double Machine::TICK_FREQUENCY;
constexpr std::chrono::nanoseconds Machine::TICK_TIME;

Machine::Machine(Program *program)
{
    this->processor = new Processor();

    //this->program_area = new UserMemoryArea("Program", 64 * 1024);
    this->program_area = new UserMemoryArea("Program", 1024 * 1024);
    processor->memory->map(0x00000000, this->program_area);
    
    this->display_area = new DisplayMemoryArea("Display", 320, 200);
    processor->memory->map(0xfffc0000, this->display_area);

    this->input_area = new RegisterMemoryArea("Input", 16);
    processor->memory->map(0xfffb0000, this->input_area);

    if (program->size() > this->program_area->size())
        throw std::runtime_error(
            std::format("Program size ({} bytes) is larger than the program area ({} bytes)",
                        program->size(), this->program_area->size())
        );
    
    this->program_area->copy_from(program->code(), program->size(), 0);
}

Machine::~Machine()
{
    delete this->processor;
}
