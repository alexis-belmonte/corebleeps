#include "include/program.hpp"

#include <fstream>
#include <stdexcept>
#include <bit>
#include <format>

using namespace CoreBleeps;

Program::Program(std::ifstream &stream)
{
    ProgramHeader header;
    stream.seekg(0, std::ios::end);
    std::streampos size = stream.tellg();

    if ((size_t) size < sizeof(header))
        throw std::runtime_error("File stream has an incomplete header");

    stream.seekg(0, std::ios::beg);
    stream.read((char *) &header, sizeof(header));

    if (std::endian::native == std::endian::little) {
        header.magic        = std::byteswap(header.magic);
        header.program_size = std::byteswap(header.program_size);
    }

    if (header.magic != HEADER_MAGIC)
        throw std::runtime_error("File header has an invalid magic value");

    this->program_name    = std::string(header.program_name);
    this->program_size    = header.program_size;
    this->program_comment = std::string(header.program_comment);

    this->program_code = new uint8_t[this->size()];
    stream.read((char *) this->program_code, this->size());
}

Program::~Program()
{
    delete this->program_code;
}

const std::string & Program::name()
{
    return this->program_name;
}

uint32_t Program::size()
{
    return this->program_size;
}

const std::string & Program::comment()
{
    return this->program_comment;
}

const uint8_t * Program::code()
{
    return this->program_code;
}
