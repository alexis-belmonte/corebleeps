#include "include/machine/instruction.hpp"

#include "include/machine/processor.hpp"

#include <cstdint>

using namespace CoreBleeps;

ProcessorOpRegisterArg::ProcessorOpRegisterArg(Processor *processor, uint8_t reg_id)
{
    this->processor = processor;
    this->reg_id    = reg_id;
}

uint32_t ProcessorOpRegisterArg::get()
{
    return this->processor->gp[this->reg_id];
}

void ProcessorOpRegisterArg::set(uint32_t value)
{
    this->processor->gp[this->reg_id] = value;
}
