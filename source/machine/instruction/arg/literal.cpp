#include "include/machine/instruction.hpp"

#include "include/machine/processor.hpp"

#include <stdexcept>

#include <cstdint>

using namespace CoreBleeps;

ProcessorOpLiteralArg::ProcessorOpLiteralArg(uint32_t literal_value)
{
    this->value = literal_value;
}

ProcessorOpLiteralArg::ProcessorOpLiteralArg(int32_t relative_value) :
    ProcessorOpLiteralArg::ProcessorOpLiteralArg(static_cast<uint32_t>(relative_value))
{}

ProcessorOpLiteralArg::ProcessorOpLiteralArg(int16_t relative_value) :
    ProcessorOpLiteralArg::ProcessorOpLiteralArg(static_cast<int32_t>(relative_value))
{}

uint32_t ProcessorOpLiteralArg::get()
{
    return this->value;
}

void ProcessorOpLiteralArg::set(uint32_t value)
{
    (void)(value);

    throw std::runtime_error("Attempt to set a value on a literal");
}
