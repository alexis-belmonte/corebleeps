#include "include/machine/instruction.hpp"

#include "include/machine/processor.hpp"

#include <cstdint>

using namespace CoreBleeps;

ProcessorOpPointerArg::ProcessorOpPointerArg(Processor *processor, uint32_t linear_address)
{
    this->processor = processor;
    this->address   = linear_address;
}

ProcessorOpPointerArg::ProcessorOpPointerArg(Processor *processor, int32_t relative_address)
{
    this->processor = processor;
    this->address   = this->processor->pc + relative_address;
}

ProcessorOpPointerArg::ProcessorOpPointerArg(Processor *processor, int16_t relative_address) :
    ProcessorOpPointerArg::ProcessorOpPointerArg(processor, static_cast<int32_t>(relative_address))
{}

uint32_t ProcessorOpPointerArg::get()
{
    return this->processor->memory->get<uint32_t>(this->address);
}

void ProcessorOpPointerArg::set(uint32_t value)
{
    this->processor->memory->set<uint32_t>(this->address, value);
}
