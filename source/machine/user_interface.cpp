#include "include/machine/user_interface.hpp"

#include "include/machine/memory.hpp"

#include <SDL2/SDL.h>

#include <stdexcept>
#include <format>

#include <cstdint>

using namespace CoreBleeps;

UserInterface::UserInterface(DisplayMemoryArea *display_area, RegisterMemoryArea *input_area, int scale)
{
    this->display_area = display_area;
    this->input_area   = input_area;

    if (SDL_Init(SDL_INIT_VIDEO) != 0)
        throw std::runtime_error(
            std::format("Failed to initialize the SDL video subsystem: {}", SDL_GetError())
        );

    this->window = SDL_CreateWindow("CoreBleeps",
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        display_area->width() * scale, display_area->height() * scale, 0);
    if (!this->window)
        throw std::runtime_error(
            std::format("Failed to create a SDL window: {}", SDL_GetError())
        );

    this->renderer =
        SDL_CreateRenderer(this->window, -1, SDL_RENDERER_ACCELERATED);
    if (!this->renderer)
        throw std::runtime_error(
            std::format("Failed to create a SDL renderer: {}", SDL_GetError())
        );

    this->area_texture = SDL_CreateTexture(this->renderer,
        SDL_PIXELFORMAT_RGBX8888, SDL_TEXTUREACCESS_STREAMING,
        this->display_area->width(), this->display_area->height());
}

UserInterface::~UserInterface()
{
    SDL_DestroyRenderer(this->renderer);
    SDL_DestroyWindow(this->window);
    SDL_Quit();
}

void UserInterface::update_area_texture()
{
    uint8_t *texture_ptr;
    int      texture_pitch;

    SDL_LockTexture(this->area_texture, NULL, (void **) &texture_ptr, &texture_pitch);
    this->display_area->copy_to(0, this->display_area->size(), texture_ptr);
    SDL_UnlockTexture(this->area_texture);
}

void UserInterface::run()
{
    SDL_Event event;
    bool      running = true;

    std::chrono::steady_clock::time_point     frame_start;
    std::chrono::duration<int64_t, std::nano> frame_time;

    while (running) {
        frame_start = std::chrono::steady_clock::now();
        
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    running = false;
                    break;
                case SDL_KEYDOWN:
                case SDL_KEYUP:
                    this->input_area->set_register(0, event.key.keysym.scancode);
                    this->input_area->set_register(1,
                        (event.key.state == SDL_PRESSED) << 0 |
                        (event.key.repeat > 0)           << 1);
                    break;
            }
        }
        
        this->update_area_texture();
        SDL_RenderCopy(this->renderer, this->area_texture, NULL, NULL);

        frame_time = std::chrono::steady_clock::now() - frame_start;

        if (frame_time < UserInterface::REFRESH_TIME)
            SDL_Delay(std::chrono::duration_cast<std::chrono::milliseconds>
                (UserInterface::REFRESH_TIME - frame_time).count());

        SDL_RenderPresent(this->renderer);
    }
}
