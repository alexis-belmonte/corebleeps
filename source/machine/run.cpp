#include "include/machine/machine.hpp"

#include "include/machine/user_interface.hpp"

#include <thread>

using namespace CoreBleeps;

void Machine::run_cpu(Machine *context)
{
    context->processor->run(Machine::TICK_TIME);
}

void Machine::run_display(Machine *context)
{
    std::unique_ptr<UserInterface> display(
        new UserInterface(context->display_area, context->input_area, 4)
    );
    display->run();
}

void Machine::run()
{
    std::thread processor_thread(Machine::run_cpu, this);
    std::thread display_thread(Machine::run_display, this);

    display_thread.join();

    this->processor->halt();
    processor_thread.join();
}
