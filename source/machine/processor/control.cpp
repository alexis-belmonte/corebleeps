#include "include/machine/processor.hpp"

#include <chrono>
#include <thread>
#include <algorithm>

using namespace CoreBleeps;

void Processor::run(std::chrono::nanoseconds tick_delay)
{
    while (this->get_flag(Processor::FLAG_RUNNING))
    {
        auto before_tick = std::chrono::steady_clock::now();
        this->tick();
        auto after_tick  = std::chrono::steady_clock::now();

        auto tick_time = after_tick - before_tick;
        if (tick_time < tick_delay)
            std::this_thread::sleep_for(std::max(std::chrono::nanoseconds::zero(), tick_delay - tick_time));
    }
}

void Processor::halt()
{
    this->set_flag(Processor::FLAG_RUNNING, false);
}
