#include "include/machine/processor.hpp"

#include "include/machine/instruction.hpp"

using namespace CoreBleeps;

const std::map<uint8_t, ProcessorOpDef> Processor::INSTRUCTION_SET = {
    {0x02, (ProcessorOpDef) {
        .name        = "ld",
        .cycle_count = 5,
        .arg_types   = {
            ARG_DIRECT | ARG_INDIRECT | ARG_WIDE,
            ARG_REGISTER
        },
        .impl        = Processor::instruction_ld
    }},
    {0x03, (ProcessorOpDef) {
        .name        = "st",
        .cycle_count = 5,
        .arg_types   = {
            ARG_REGISTER,
            ARG_REGISTER | ARG_REGISTER_RELATIVE | ARG_INDIRECT
        },
        .impl        = Processor::instruction_st
    }},
    {0x04, (ProcessorOpDef) {
        .name        = "add",
        .cycle_count = 10,
        .arg_types   = {
            ARG_REGISTER,
            ARG_REGISTER,
            ARG_REGISTER
        },
        .impl        = Processor::instruction_add
    }},
    {0x05, (ProcessorOpDef) {
        .name        = "sub",
        .cycle_count = 10,
        .arg_types   = {
            ARG_REGISTER,
            ARG_REGISTER,
            ARG_REGISTER
        },
        .impl        = Processor::instruction_sub
    }},
    {0x06, (ProcessorOpDef) {
        .name        = "and",
        .cycle_count = 6,
        .arg_types   = {
            ARG_REGISTER | ARG_DIRECT | ARG_INDIRECT | ARG_WIDE,
            ARG_REGISTER | ARG_DIRECT | ARG_INDIRECT | ARG_WIDE,
            ARG_REGISTER
        },
        .impl        = Processor::instruction_and
    }},
    {0x07, (ProcessorOpDef) {
        .name        = "or",
        .cycle_count = 6,
        .arg_types   = {
            ARG_REGISTER | ARG_DIRECT | ARG_INDIRECT | ARG_WIDE,
            ARG_REGISTER | ARG_DIRECT | ARG_INDIRECT | ARG_WIDE,
            ARG_REGISTER
        },
        .impl        = Processor::instruction_or
    }},
    {0x08, (ProcessorOpDef) {
        .name        = "xor",
        .cycle_count = 6,
        .arg_types   = {
            ARG_REGISTER | ARG_DIRECT | ARG_INDIRECT | ARG_WIDE,
            ARG_REGISTER | ARG_DIRECT | ARG_INDIRECT | ARG_WIDE,
            ARG_REGISTER
        },
        .impl        = Processor::instruction_xor
    }},
    {0x09, (ProcessorOpDef) {
        .name        = "zjmp",
        .cycle_count = 20,
        .arg_types   = {
            ARG_DIRECT | ARG_DIRECT_RELATIVE
        },
        .impl        = Processor::instruction_zjmp
    }},
    {0x0a, (ProcessorOpDef) {
        .name        = "ldi",
        .cycle_count = 25,
        .arg_types   = {
            ARG_REGISTER | ARG_DIRECT | ARG_INDIRECT,
            ARG_REGISTER | ARG_DIRECT,
            ARG_REGISTER
        },
        .impl        = Processor::instruction_ldi
    }},
    {0x0b, (ProcessorOpDef) {
        .name        = "sti",
        .cycle_count = 25,
        .arg_types   = {
            ARG_REGISTER,
            ARG_REGISTER | ARG_DIRECT | ARG_INDIRECT,
            ARG_REGISTER | ARG_DIRECT
        },
        .impl        = Processor::instruction_sti
    }},
    {0x11, (ProcessorOpDef) {
        .name        = "nop",
        .cycle_count = 5,
        .arg_types   = {
            ARG_REGISTER | ARG_DIRECT | ARG_INDIRECT,
            ARG_REGISTER
        },
        .impl        = Processor::instruction_nop
    }},
    {0x12, (ProcessorOpDef) {
        .name        = "push",
        .cycle_count = 20,
        .arg_types   = {
            ARG_REGISTER | ARG_DIRECT | ARG_INDIRECT | ARG_WIDE
        },
        .impl        = Processor::instruction_push
    }},
    {0x13, (ProcessorOpDef) {
        .name        = "pop",
        .cycle_count = 20,
        .arg_types   = {
            ARG_REGISTER | ARG_INDIRECT
        },
        .impl        = Processor::instruction_pop
    }},
    {0x14, (ProcessorOpDef) {
        .name        = "call",
        .cycle_count = 20,
        .arg_types   = {
            ARG_REGISTER | ARG_DIRECT | ARG_WIDE | ARG_INDIRECT
        },
        .impl        = Processor::instruction_call
    }},
    {0x15, (ProcessorOpDef) {
        .name        = "ret",
        .cycle_count = 20,
        .arg_types   = {},
        .impl        = Processor::instruction_ret
    }},
    {0x16, (ProcessorOpDef) {
        .name        = "lsp",
        .cycle_count = 5,
        .arg_types   = {
            ARG_REGISTER | ARG_DIRECT | ARG_WIDE | ARG_INDIRECT
        },
        .impl        = Processor::instruction_lsp
    }},
    {0x17, (ProcessorOpDef) {
        .name        = "ssp",
        .cycle_count = 5,
        .arg_types   = {
            ARG_REGISTER
        },
        .impl        = Processor::instruction_ssp
    }},
    {0x18, (ProcessorOpDef) {
        .name        = "jmp",
        .cycle_count = 15,
        .arg_types   = {
            ARG_DIRECT | ARG_DIRECT_RELATIVE | ARG_INDIRECT | ARG_REGISTER | ARG_REGISTER_RELATIVE
        },
        .impl        = Processor::instruction_jmp
    }},
    {0x19, (ProcessorOpDef) {
        .name        = "nzjmp",
        .cycle_count = 20,
        .arg_types   = {
            ARG_DIRECT | ARG_DIRECT_RELATIVE | ARG_INDIRECT | ARG_REGISTER
        },
        .impl        = Processor::instruction_nzjmp
    }},
    {0x1a, (ProcessorOpDef) {
        .name        = "cjmp",
        .cycle_count = 20,
        .arg_types   = {
            ARG_DIRECT | ARG_DIRECT_RELATIVE | ARG_INDIRECT | ARG_REGISTER
        },
        .impl        = Processor::instruction_cjmp
    }},
    {0x1b, (ProcessorOpDef) {
        .name        = "shl",
        .cycle_count = 5,
        .arg_types   = {
            ARG_DIRECT | ARG_INDIRECT | ARG_REGISTER,
            ARG_DIRECT | ARG_REGISTER,
            ARG_REGISTER
        },
        .impl        = Processor::instruction_shl
    }},
    {0x1c, (ProcessorOpDef) {
        .name        = "shr",
        .cycle_count = 5,
        .arg_types   = {
            ARG_DIRECT | ARG_INDIRECT | ARG_REGISTER,
            ARG_DIRECT | ARG_REGISTER,
            ARG_REGISTER
        },
        .impl        = Processor::instruction_shr
    }},
    {0x1d, (ProcessorOpDef) {
        .name        = "not",
        .cycle_count = 5,
        .arg_types   = {
            ARG_DIRECT | ARG_INDIRECT | ARG_REGISTER,
            ARG_REGISTER
        },
        .impl        = Processor::instruction_not
    }},
    {0x1e, (ProcessorOpDef) {
        .name        = "cmp",
        .cycle_count = 25,
        .arg_types   = {
            ARG_INDIRECT | ARG_REGISTER,
            ARG_DIRECT | ARG_WIDE | ARG_INDIRECT | ARG_REGISTER
        },
        .impl        = Processor::instruction_cmp
    }},
    {0x1f, (ProcessorOpDef) {
        .name        = "ldf",
        .cycle_count = 10,
        .arg_types   = {
            ARG_REGISTER | ARG_DIRECT | ARG_WIDE,
            ARG_REGISTER
        },
        .impl        = Processor::instruction_ldf
    }},
    {0x20, (ProcessorOpDef) {
        .name        = "stf",
        .cycle_count = 10,
        .arg_types   = {
            ARG_REGISTER | ARG_DIRECT | ARG_WIDE,
            ARG_REGISTER | ARG_DIRECT | ARG_WIDE
        },
        .impl        = Processor::instruction_stf
    }},
    {0x21, (ProcessorOpDef) {
        .name        = "clc",
        .cycle_count = 5,
        .arg_types   = {},
        .impl        = Processor::instruction_clc
    }},
    {0x22, (ProcessorOpDef) {
        .name        = "stc",
        .cycle_count = 5,
        .arg_types   = {},
        .impl        = Processor::instruction_stc
    }},
    {0x23, (ProcessorOpDef) {
        .name        = "gjmp",
        .cycle_count = 25,
        .arg_types   = {
            ARG_DIRECT | ARG_DIRECT_RELATIVE | ARG_INDIRECT | ARG_REGISTER
        },
        .impl        = Processor::instruction_gjmp
    }},
    {0x24, (ProcessorOpDef) {
        .name        = "ljmp",
        .cycle_count = 25,
        .arg_types   = {
            ARG_DIRECT | ARG_DIRECT_RELATIVE | ARG_INDIRECT | ARG_REGISTER
        },
        .impl        = Processor::instruction_ljmp
    }},
    {0x25, (ProcessorOpDef) {
        .name        = "ejmp",
        .cycle_count = 25,
        .arg_types   = {
            ARG_DIRECT | ARG_DIRECT_RELATIVE | ARG_INDIRECT | ARG_REGISTER
        },
        .impl        = Processor::instruction_ejmp
    }},
    {0x26, (ProcessorOpDef) {
        .name        = "inc",
        .cycle_count = 5,
        .arg_types   = {
            ARG_REGISTER
        },
        .impl        = Processor::instruction_inc
    }},
    {0x27, (ProcessorOpDef) {
        .name        = "dec",
        .cycle_count = 5,
        .arg_types   = {
            ARG_REGISTER
        },
        .impl        = Processor::instruction_dec
    }}
};
