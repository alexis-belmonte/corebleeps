#include "include/machine/processor.hpp"

#include "include/machine/instruction.hpp"

#include <list>
#include <format>
#include <stdexcept>

#include <cstdint>

using namespace CoreBleeps;

ProcessorOpArgs Processor::decode()
{
    std::list<ProcessorOpArgType> actual_arg_types;
    ProcessorOpArgs               values;
    size_t                        pc = this->pc + 1;

    if (this->opcode->arg_types.size() == 1 &&
        *this->opcode->arg_types.begin() & ProcessorOpArgType::ARG_DIRECT)
        actual_arg_types.push_back(ProcessorOpArgType::ARG_DIRECT);
    else if (this->opcode->arg_types.size() >= 1) {
        uint8_t pcb = this->memory->get<uint8_t>(pc++);
        for (int i = 3; i >= 0; i--) {
            uint8_t            raw_type = (pcb >> i * 2) & 0b11;
            ProcessorOpArgType type;

            switch (raw_type) {
                case 0b00: default:
                    break;
                case 0b01: type = ProcessorOpArgType::ARG_REGISTER;
                    break;
                case 0b10: type = ProcessorOpArgType::ARG_DIRECT;
                    break;
                case 0b11: type = ProcessorOpArgType::ARG_INDIRECT;
                    break;
            }

            if (raw_type == 0b00)
                break;
            else
                actual_arg_types.push_back(type);
        }
    }

    if (actual_arg_types.size() != this->opcode->arg_types.size())
        throw std::runtime_error(
            std::format("Instruction '{}' has a mismatched number of arguments (received {}, but expected {})",
                        this->opcode->name, actual_arg_types.size(), this->opcode->arg_types.size())
        );
    
    auto actual_arg_types_it  = actual_arg_types.begin();
    auto defined_arg_types_it = this->opcode->arg_types.begin();
    for (int i = 0; actual_arg_types_it != actual_arg_types.end();
        actual_arg_types_it++, defined_arg_types_it++, i++) {
        if ((*actual_arg_types_it & *defined_arg_types_it) == 0)
            throw std::runtime_error(
                std::format("Instruction '{}' has an unexpected argument type at #{}", this->opcode->name, i)
            );

        switch (*actual_arg_types_it) {
            case ProcessorOpArgType::ARG_REGISTER: {
                uint8_t  reg_id   = this->memory->get<uint8_t>(pc++) - 1;
                bool     relative = *defined_arg_types_it & ProcessorOpArgType::ARG_REGISTER_RELATIVE;

                if (reg_id > sizeof(this->gp) / sizeof(*this->gp))
                    throw std::runtime_error(
                        std::format("Instruction '{}' has an unexpected register argument number at #{}",
                                    this->opcode->name, i)
                    );
                
                if (relative)
                    values.push_back(std::shared_ptr<ProcessorOpArg>(
                        new ProcessorOpPointerArg(this, (int32_t) this->gp[reg_id])
                    ));
                else
                    values.push_back(std::shared_ptr<ProcessorOpArg>(
                        new ProcessorOpRegisterArg(this, reg_id)
                    ));
                break;
            }
            
            case ProcessorOpArgType::ARG_DIRECT: {
                uint32_t value;
                bool     wide     = *defined_arg_types_it & ProcessorOpArgType::ARG_WIDE;
                bool     relative = *defined_arg_types_it & ProcessorOpArgType::ARG_DIRECT_RELATIVE;

                if (wide) {
                    value = this->memory->get<uint32_t>(pc);
                    pc += 4;
                } else {
                    value = this->memory->get<uint16_t>(pc);
                    pc += 2;
                }

                if (relative)
                    value = this->pc + (wide ? (int32_t) value : (int16_t) value);

                values.push_back(std::shared_ptr<ProcessorOpArg>(new ProcessorOpLiteralArg(value)));
                break;
            }

            case ProcessorOpArgType::ARG_INDIRECT: {
                int16_t rel_address = this->memory->get<int16_t>(pc);
                bool    use_value   = *defined_arg_types_it & ProcessorOpArgType::ARG_INDIRECT_VALUE;

                pc += 2;
                if (use_value)
                    values.push_back(std::shared_ptr<ProcessorOpArg>(new ProcessorOpLiteralArg(rel_address)));
                else
                    values.push_back(std::shared_ptr<ProcessorOpArg>(new ProcessorOpPointerArg(this, rel_address)));
                break;
            }

            default:
                throw std::runtime_error("The decoder reached an invalid state");
        }
    }

    this->opcode_length = pc - this->pc;
    return values;
}
