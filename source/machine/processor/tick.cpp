#include "include/machine/processor.hpp"

using namespace CoreBleeps;

void Processor::tick()
{
    if      (!this->get_flag(Processor::FLAG_RUNNING))
        return;
    else if (this->cycles_left > 0)
        this->cycles_left--;
    else if (!this->opcode_invalid && !this->opcode)
        this->fetch();
    else
        this->step();
}
