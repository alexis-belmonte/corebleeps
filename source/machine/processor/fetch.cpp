#include "include/machine/processor.hpp"

#include <cstdint>

using namespace CoreBleeps;

void Processor::fetch()
{
    uint8_t opcode = this->memory->get<uint8_t>(this->pc);
    const ProcessorOpDef *instruction;

    this->opcode_invalid = !Processor::INSTRUCTION_SET.contains(opcode);

    if (this->opcode_invalid) {
        instruction = nullptr;
        this->cycles_left = 5;
    } else {
        instruction = &Processor::INSTRUCTION_SET.find(opcode)->second;
        this->cycles_left = instruction->cycle_count;
    }

    this->opcode = instruction;
}
