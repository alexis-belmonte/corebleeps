#include "include/machine/processor.hpp"

#include <algorithm>

#include <cstddef>
#include <cstdint>

using namespace CoreBleeps;

const uint32_t Processor::FLAG_CARRY;
const uint32_t Processor::FLAG_ZERO;
const uint32_t Processor::FLAG_EQUAL;
const uint32_t Processor::FLAG_GREATER;
const uint32_t Processor::FLAG_RUNNING;

Processor::Processor()
{
    this->pc = 0;
    this->sp = 0;
    std::fill_n(this->gp, sizeof(this->gp) / sizeof(*this->gp), 0);
    this->flags = 0;

    this->opcode_invalid = false;
    this->opcode         = nullptr;
    this->cycles_left    = 0;
    
    this->memory = new MemoryMap();

    this->set_flag(Processor::FLAG_RUNNING, true);
}

Processor::~Processor()
{
    delete this->memory;
}
