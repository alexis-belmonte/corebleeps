#include "include/machine/processor.hpp"

#include <list>

using namespace CoreBleeps;

bool Processor::instruction_stf(Processor *processor, ProcessorOpArgs args)
{
    uint32_t value   = args.front()->get();
    uint32_t address = args.back()->get();

    processor->memory->set<uint32_t>(address, value);

    processor->set_flag(Processor::FLAG_ZERO, value == 0);

    return true;
}
