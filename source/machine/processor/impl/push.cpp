#include "include/machine/processor.hpp"

#include <list>

using namespace CoreBleeps;

bool Processor::instruction_push(Processor *processor, ProcessorOpArgs args)
{
    uint32_t value = args.front()->get();
    
    processor->sp -= 4;
    processor->memory->set<uint32_t>(processor->sp, value);

    return true;
}
