#include "include/machine/processor.hpp"

#include <list>

using namespace CoreBleeps;

bool Processor::instruction_ldf(Processor *processor, ProcessorOpArgs args)
{
    uint32_t address = args.front()->get();
    uint32_t value   = processor->memory->get<uint32_t>(address);

    args.back()->set(value);

    processor->set_flag(Processor::FLAG_ZERO, value == 0);

    return true;
}
