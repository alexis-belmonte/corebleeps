#include "include/machine/processor.hpp"

#include <list>

using namespace CoreBleeps;

bool Processor::instruction_ssp(Processor *processor, ProcessorOpArgs args)
{
    uint32_t value = processor->sp;

    (*args.begin())->set(value);

    return true;
}
