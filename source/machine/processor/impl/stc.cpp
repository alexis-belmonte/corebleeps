#include "include/machine/processor.hpp"

#include <list>

using namespace CoreBleeps;

bool Processor::instruction_stc(Processor *processor, ProcessorOpArgs args)
{
    (void)(args);

    processor->set_flag(Processor::FLAG_CARRY, true);

    return true;
}
