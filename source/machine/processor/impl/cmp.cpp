#include "include/machine/processor.hpp"

#include <list>

using namespace CoreBleeps;

bool Processor::instruction_cmp(Processor *processor, ProcessorOpArgs args)
{
    uint32_t a = args.front()->get();
    uint32_t b = args.back()->get();

    processor->set_flag(Processor::FLAG_CARRY,   (a & b) == 0);
    processor->set_flag(Processor::FLAG_EQUAL,   a == b);
    processor->set_flag(Processor::FLAG_GREATER, a > b);

    return true;
}
