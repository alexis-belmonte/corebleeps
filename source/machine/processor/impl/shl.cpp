#include "include/machine/processor.hpp"

#include <list>

using namespace CoreBleeps;

bool Processor::instruction_shl(Processor *processor, ProcessorOpArgs args)
{
    auto     arg_iterator = args.begin();
    uint32_t value  = (*arg_iterator++)->get();
    uint32_t shift  = (*arg_iterator++)->get();
    uint32_t result = value << shift;

    processor->set_flag(Processor::FLAG_ZERO, result == 0);

    (*arg_iterator)->set(result);

    return true;
}
