#include "include/machine/processor.hpp"

#include <list>

using namespace CoreBleeps;

bool Processor::instruction_clc(Processor *processor, ProcessorOpArgs args)
{
    (void)(args);

    processor->set_flag(Processor::FLAG_CARRY, false);

    return true;
}
