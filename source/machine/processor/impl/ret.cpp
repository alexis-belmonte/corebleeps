#include "include/machine/processor.hpp"

#include <list>

using namespace CoreBleeps;

bool Processor::instruction_ret(Processor *processor, ProcessorOpArgs args)
{
    (void)(args);

    uint32_t previous_address = processor->memory->get<uint32_t>(processor->sp);

    processor->sp += 4;
    processor->pc = previous_address;

    return false;
}
