#include "include/machine/processor.hpp"

#include <list>

using namespace CoreBleeps;

bool Processor::instruction_sti(Processor *processor, ProcessorOpArgs args)
{
    auto     arg_iterator = args.begin();

    uint32_t value          = (*arg_iterator++)->get();
    uint32_t base           = (*arg_iterator++)->get();
    uint32_t rel            = (*arg_iterator++)->get();
    uint32_t linear_address = processor->pc + base + rel;

    processor->memory->set<uint32_t>(linear_address, value);

    processor->set_flag(Processor::FLAG_ZERO, value == 0);

    return true;
}
