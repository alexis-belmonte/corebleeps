#include "include/machine/processor.hpp"

#include <list>

using namespace CoreBleeps;

bool Processor::instruction_st(Processor *processor, ProcessorOpArgs args)
{
    (void)(processor);

    args.back()->set(args.front()->get());

    return true;
}
