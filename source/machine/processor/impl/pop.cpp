#include "include/machine/processor.hpp"

#include <list>

using namespace CoreBleeps;

bool Processor::instruction_pop(Processor *processor, ProcessorOpArgs args)
{
    uint32_t value = processor->memory->get<uint32_t>(processor->sp);

    processor->sp += 4;
    (*args.begin())->set(value);

    return true;
}
