#include "include/machine/processor.hpp"

#include <list>

using namespace CoreBleeps;

bool Processor::instruction_lsp(Processor *processor, ProcessorOpArgs args)
{
    uint32_t value = args.front()->get();

    processor->sp = value;

    processor->set_flag(Processor::FLAG_ZERO, value == 0);

    return true;
}
