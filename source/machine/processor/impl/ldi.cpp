#include "include/machine/processor.hpp"

#include <list>

using namespace CoreBleeps;

bool Processor::instruction_ldi(Processor *processor, ProcessorOpArgs args)
{
    auto     arg_iterator = args.begin();

    uint32_t base           = (*arg_iterator++)->get();
    uint32_t rel            = (*arg_iterator++)->get();
    uint32_t linear_address = processor->pc + base + rel;
    uint32_t value          = processor->memory->get<uint32_t>(linear_address);

    (*arg_iterator)->set(value);

    processor->set_flag(Processor::FLAG_ZERO, value == 0);

    return true;
}
