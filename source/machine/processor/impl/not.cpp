#include "include/machine/processor.hpp"

#include <list>

using namespace CoreBleeps;

bool Processor::instruction_not(Processor *processor, ProcessorOpArgs args)
{
    uint32_t result = ~(args.front()->get());

    args.back()->set(result);

    processor->set_flag(Processor::FLAG_ZERO, result == 0);

    return true;
}
