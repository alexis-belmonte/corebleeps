#include "include/machine/processor.hpp"

#include <list>

#include <climits>

using namespace CoreBleeps;

bool Processor::instruction_add(Processor *processor, ProcessorOpArgs args)
{
    auto     arg_iterator = args.begin();
    uint32_t left   = (*arg_iterator++)->get();
    uint32_t right  = (*arg_iterator++)->get();
    uint32_t result = left + right;

    processor->set_flag(Processor::FLAG_ZERO,  result == 0);
    processor->set_flag(Processor::FLAG_CARRY, (left > 0 && left > UINT_MAX - right));

    (*arg_iterator)->set(result);

    return true;
}
