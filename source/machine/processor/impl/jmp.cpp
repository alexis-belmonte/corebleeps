#include "include/machine/processor.hpp"

#include <list>

using namespace CoreBleeps;

bool Processor::instruction_jmp(Processor *processor, ProcessorOpArgs args)
{
    (void)(processor);

    processor->pc = args.front()->get();

    return false;
}
