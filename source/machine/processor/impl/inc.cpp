#include "include/machine/processor.hpp"

#include <list>

using namespace CoreBleeps;

bool Processor::instruction_inc(Processor *processor, ProcessorOpArgs args)
{
    uint32_t value = args.front()->get();

    args.front()->set(value + 1);

    processor->set_flag(Processor::FLAG_CARRY, value == (uint32_t) ~0);

    return true;
}
