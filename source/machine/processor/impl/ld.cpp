#include "include/machine/processor.hpp"

#include <list>

using namespace CoreBleeps;

bool Processor::instruction_ld(Processor *processor, ProcessorOpArgs args)
{
    uint32_t value = args.front()->get();

    args.back()->set(value);

    processor->set_flag(Processor::FLAG_ZERO, value == 0);

    return true;
}
