#include "include/machine/processor.hpp"

#include <list>

using namespace CoreBleeps;

bool Processor::instruction_or(Processor *processor, ProcessorOpArgs args)
{
    auto     arg_iterator = args.begin();
    uint32_t result = (*arg_iterator++)->get() | (*arg_iterator++)->get();

    (*arg_iterator)->set(result);

    processor->set_flag(Processor::FLAG_ZERO, result == 0);

    return true;
}
