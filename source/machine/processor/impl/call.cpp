#include "include/machine/processor.hpp"

#include <list>

using namespace CoreBleeps;

bool Processor::instruction_call(Processor *processor, ProcessorOpArgs args)
{
    processor->sp -= 4;
    processor->memory->set<uint32_t>(processor->sp, processor->pc + processor->opcode_length);

    processor->pc += args.front()->get();

    return false;
}
