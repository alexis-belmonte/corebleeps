#include "include/machine/processor.hpp"

#include <list>

using namespace CoreBleeps;

bool Processor::instruction_ejmp(Processor *processor, ProcessorOpArgs args)
{
    if (processor->get_flag(Processor::FLAG_EQUAL)) {
        processor->pc = args.front()->get();
        return false;
    } else
        return true;
}
