#include "include/machine/processor.hpp"

#include <list>

using namespace CoreBleeps;

bool Processor::instruction_nop(Processor *processor, ProcessorOpArgs args)
{
    (void)(processor);
    (void)(args);

    return true;
}
