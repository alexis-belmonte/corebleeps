#include "include/machine/processor.hpp"

#include <format>
#include <stdexcept>

using namespace CoreBleeps;

void Processor::step()
{
    if      (!this->get_flag(Processor::FLAG_RUNNING))
        return;
    else if (!this->opcode_invalid && !this->opcode)
        this->fetch();

    if (this->opcode_invalid) {
        this->opcode_invalid = false;
        this->pc++;
        return;
    }

    if (!this->opcode->impl)
        throw std::runtime_error(
            std::format("Instruction '{}' not implemented", this->opcode->name)
        );
    if (this->opcode->impl(this, this->decode()))
        this->pc += this->opcode_length;
    this->opcode = nullptr;
}
