#include "include/machine/memory.hpp"

#include <cstdint>
#include <cstddef>

using namespace CoreBleeps;

RegisterMemoryArea::RegisterMemoryArea(std::string name, int count) :
    UserMemoryArea::UserMemoryArea(name, count * sizeof(uint32_t))
{}

uint32_t RegisterMemoryArea::get_register(int id)
{
    uint32_t value = 0;

    for (size_t i = 0; i < sizeof(uint32_t); i++) {
        value <<= 8;
        value |= this->get(id * 4 + i);
    }
    return value;
}

void RegisterMemoryArea::set_register(int id, uint32_t value)
{
    for (size_t i = 0; i < sizeof(uint32_t); i++)
        this->set(id * 4 + i, value >> (3 - i) * 8);
}
