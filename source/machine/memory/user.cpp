#include "include/machine/memory.hpp"

#include <algorithm>

using namespace CoreBleeps;

UserMemoryArea::UserMemoryArea(std::string name, uint32_t size)
{
    this->my_name     = name;
    this->my_mem_size = size;
    this->my_mem      = new uint8_t[size];

    std::fill_n(this->my_mem, size, 0);
}

UserMemoryArea::~UserMemoryArea()
{
    delete[] this->my_mem;
}

std::string & UserMemoryArea::name()
{
    return this->my_name;
}

uint32_t UserMemoryArea::size()
{
    return this->my_mem_size;
}

uint8_t UserMemoryArea::get(uint32_t address)
{
    return this->my_mem[address];
}

void UserMemoryArea::set(uint32_t address, uint8_t value)
{
    this->my_mem[address] = value;
}

void UserMemoryArea::copy_from(const uint8_t *buffer, uint32_t size, uint32_t target_address)
{
    std::copy_n(buffer, size, &this->my_mem[target_address]);
}

void UserMemoryArea::copy_to(uint32_t source_address, uint32_t size, uint8_t *buffer)
{
    std::copy_n(&this->my_mem[source_address], size, buffer);
}
