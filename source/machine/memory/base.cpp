#include "include/machine/memory.hpp"

#include <list>
#include <tuple>
#include <stdexcept>
#include <format>

#include <cstdint>

using namespace CoreBleeps;

using space_entry = std::tuple<uint32_t, MemoryArea *>;

const uint8_t MemoryMap::DEFAULT_RAM_READVALUE;

MemoryMap::MemoryMap()
{
    this->space = new std::list<space_entry>();
}

MemoryMap::~MemoryMap()
{
    for (space_entry const &entry : *this->space)
        delete std::get<1>(entry);
    delete this->space;
}

void MemoryMap::map(uint32_t starting_address, MemoryArea *area)
{
    for (space_entry const &entry : *this->space) {
        uint32_t other_area_address   = std::get<0>(entry);
        MemoryArea * const other_area = std::get<1>(entry);

        if (other_area_address >= starting_address &&
            other_area_address + other_area->size() <= starting_address + area->size())
            throw std::runtime_error(
                std::format("Area conflict with {} starting at 0x{:x}",
                            std::get<1>(entry)->name(), other_area_address)
            );
    }

    this->space->push_back(std::make_tuple(starting_address, area));
}

void MemoryMap::unmap(uint32_t starting_address)
{
    for (auto entry = this->space->cbegin(); entry != this->space->cend();) {
        uint32_t area_address = std::get<0>(*entry);

        if (area_address == starting_address) {
            entry = this->space->erase(entry);
            return;
        }
    }
}

const space_entry * MemoryMap::get_area(uint32_t address) const
{
    for (space_entry const &entry : *this->space) {
        uint32_t area_address = std::get<0>(entry);

        if (area_address <= address && address <= area_address + std::get<1>(entry)->size())
            return &entry;
    }

    return nullptr;
}
