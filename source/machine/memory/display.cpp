#include "include/machine/memory.hpp"

using namespace CoreBleeps;

DisplayMemoryArea::DisplayMemoryArea(std::string name, int width, int height) :
    UserMemoryArea::UserMemoryArea(name, width * height * 4)
{
    this->my_width  = width;
    this->my_height = height;
}

int DisplayMemoryArea::width()
{
    return this->my_width;
}

int DisplayMemoryArea::height()
{
    return this->my_height;
}
