# CoreBleeps

## Introduction

CoreBleeps is a super-set of the original CoreWar virtual machine; that is, it
extends on the instruction set and adds around 30 instructions. It also
proposes a full fledged 32-bit address space, that is both flexible and
extensible.

This project has been developped in C++, and uses SDL2 as the backend for
graphics.

## Compile the Project

### Requirements

To compile this project, you will need:

- A recent C++ compiler which supports the latest C++23 standard, preferrably
  `g++`
- The SDL2 library, version 2.26.2

### Compile it

The only thing left to do is just

```bash
make -j$(nproc)
```

This will compile the project with all of the available cores.

## Test the Project

There is currently a CoreWar assembler written in C that also supports the
extended instruction set. However, due to the fact that this is a school
project, I am not allowed to publish the source code for it.

I'll rewrite an assembler in C++ if I have time -- In the mean-time, I'll
write ASAP the instruction set, as well as the current memory mapping that
is subject to change.

In the mean time, you can test the emulator by just running `./corebleeps`.
You can try the bonus program which comes with this project. It basically
converts a given image into a stream of instructions which directly writes to
the screen area.

## Speed

The processor is currently tuned to run at 20 MHz, which is pretty much the
speed that my computer can handle without starting to slow down.
